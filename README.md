# Racket

[racket-lang.org](https://racket-lang.org/)
Extensible programming language in the Scheme family

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=sbcl+ecl+maxima-sage+racket+clojure+chezscheme+smlnj+picolisp+newlisp&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%Y)

# Books
* https://racket-lang.org/books.html
---
* *Racket Programming the Fun Way* 2021-01 James W. Stelly
  * https://nostarch.com/racket-programming-fun-way
  * https://www.oreilly.com/library/view/racket-programming-the/9781098128449/
* *Don’t Teach Coding: Until You Read this Book* 2020
  * https://www.wiley.com/en-us/Don%27t+Teach+Coding%3A+Until+You+Read+This+Book-p-9781119602637

# Languages as Racket extentions
## Programming Languages
### Clojure
* [racket-clojure](https://github.com/takikawa/racket-clojure) 2015
  \#lang clojure
* [rackjure](https://pkgs.racket-lang.org/package/rackjure) 2014
  Some Clojure-inspired idioms.

### Forge
* [forge](https://pkgs.racket-lang.org/package/forge) 2022
  A model-finding language with Alloy-adjacent syntax.

### Forth
* [forth](https://pkgs.racket-lang.org/package/forth) 2015
  Forth emulator, as a Racket #lang

### Functional programming (not a language)
* [functional](https://pkgs.racket-lang.org/package/functional) 2016
  generic interfaces and data structures for functional programming

### Hackett
* [hackett](https://pkgs.racket-lang.org/package/hackett) 2019
  Statically typed, pure, lazy, functional programming
* [lexi-lambda/hackett](https://github.com/lexi-lambda/hackett)
* Work in progress! (no activity 2020-2024), may need old version of `racket`.

### J
* [j](https://pkgs.racket-lang.org/package/j) 2017
  Racket implementation of J language and related concepts

### Java
* [profj](https://pkgs.racket-lang.org/package/profj) 2013
  ProfessorJ

### Lisp
* [sicp](https://pkgs.racket-lang.org/package/sicp) 2019
  SICP Support for DrRacket

### Pascal
* [minipascal](https://pkgs.racket-lang.org/package/minipascal) 2013
  MiniPascal as a Racket language

### Pie
* [pie-a-let-mode](https://pkgs.racket-lang.org/package/pie-a-let-mode) 2018
  A little fork of the Pie language (adds `let` and `equal`).

### Plai
The language that accompanies Krishnamurthi's PLAI book.
* [plaitypus](https://pkgs.racket-lang.org/package/plaitypus) 2019
  Typed variant of #lang plai. Forked from plai-typed, brought closer to #lang plai.
* [play](https://pkgs.racket-lang.org/package/play) 2019
  Variations on #lang plai, the language that accompanies Krishnamurthi's PLAI book.
* [plait](https://pkgs.racket-lang.org/package/plait) 2018
  The Plait language is a hybrid of Racket (concrete syntax) and ML (type system) that is intended for use in an interpreters-based programming-languages course. It's an improved variant of `plai-typed`.
* [plai-lazy](https://pkgs.racket-lang.org/package/plai-lazy) 2015
  A lazy variant of the plai language
* [plai-typed](https://pkgs.racket-lang.org/package/plai-typed) 2014
  The `plai-typed' language for use with Programming Languages: Application and Interpretation, Second Edition

### Python
* [python](https://pkgs.racket-lang.org/package/python) 2015
  An implementation of the Python programming language for Racket

### Qi
* [qi](https://pkgs.racket-lang.org/package/qi) 2022
  A general-purpose functional DSL.

### Rosette
* [rosette](https://pkgs.racket-lang.org/package/rosette) 2016
  The Rosette Language

### scratchy
* [scratchy](https://pkgs.racket-lang.org/package/scratchy) 2012
  Scratch-like runtime and language

#### Taro
* [taro](https://pkgs.racket-lang.org/package/taro) 2021
  Taro lang

### Turnstile
* [turnstile](https://pkgs.racket-lang.org/package/turnstile) 2018
  #lang for implementing typed languages with Racket

### Typed Racket
* [typed-racket](https://pkgs.racket-lang.org/package/typed-racket) 2014-2024
* [racket/typed-racket](https://github.com/racket/typed-racket/)
* [*The Typed Racket Guide*
  ](https://docs.racket-lang.org/ts-guide/)
* [*How good is Typed Racket?*
](https://www.reddit.com/r/lisp/comments/1ajms7n/how_good_is_typed_racket/) (Reddit)
* [Coalton](https://github.com/coalton-lang/coalton) Coalton is an efficient, statically typed functional programming language that supercharges Common Lisp

### Wort
* [wort](https://pkgs.racket-lang.org/package/wort) 2017
  Tiny concatenative language with polymorphic type inference

## Backends
### C
* [rmc](https://pkgs.racket-lang.org/package/rmc) 2016
  Racket-made C

* [fulmar](https://pkgs.racket-lang.org/package/fulmar) 2014
  Generate C++ code from S-expressions

* [superc](https://pkgs.racket-lang.org/package/superc) 2012
  Language for writing C with Racket macros and an easy interface to Racket

### JavaScript
* [racketscript](https://pkgs.racket-lang.org/package/racketscript) 2021
  Lightweight Racket to JavaScript compiler with some batteries included

### SQL
* [plisqin](https://pkgs.racket-lang.org/package/plisqin) 2019
  SQL generator
* [sql](https://pkgs.racket-lang.org/package/sql) 2016
  an S-expression notation for SQL

# Some libraries
## Parsing
* [megaparsack](https://pkgs.racket-lang.org/package/megaparsack) 2016
  practical parser combinators
